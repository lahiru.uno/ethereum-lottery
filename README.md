## Blockchain

1. _cd blockchain_ 
2. In terminal execute _truffle develop_
3. in trufflr development console run _migrate --reset_ - deploy smart contract

## React
1. _cd client_
2. run _npm install_ to install all dependencies
3. set the contract address as contractAddr in App.js
4. copy the Abi from build directory smart contract js file to abi/abis.js file
5. connect metamask to Ganache
6. run _npm start_ to run the server
7. browse _localhost:3000_ view the app.

## Metamask

Connect several accounts to react app.
Try to submit ballots by changing the metamask active accout. this will add different accounts as participants for the lottery.