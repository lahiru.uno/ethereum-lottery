// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

contract Lottery {
    address public manager;
    address payable[] public players;
    uint public winner;

    constructor() {
        manager = msg.sender;
    }

    event NewBallot( 
        address _client,
        string _message
    );
    
    modifier isManager{
        require(msg.sender == manager, "Only manager can call this function.");
        _;
    }

    function submitBallot() public payable {
        require(msg.value >= 1 ether);
        players.push(payable(msg.sender));
        emit NewBallot(msg.sender, 'New Ballot submitted');
    }

    function random() private view returns (uint) {
        return uint(sha256(abi.encodePacked(block.difficulty, block.timestamp, players)));
    }

    function getPlayersBalance(uint index) public view 
        returns (uint256) {
        return players[index].balance;
    }
    
    function getBalance() public view returns(uint256){
        return address(this).balance;
    }

    function pickWinner() public payable isManager{
        uint index = random() % players.length;
        winner = index;
        players[index].transfer(address(this).balance);
        players = new address payable[](0);
    }
}