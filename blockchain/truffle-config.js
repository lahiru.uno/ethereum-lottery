module.exports = {
  networks: {
    development: {
      network_id: "*",
      host: "localhost",
      port: 7545,
      from: '0x46fa3D4Fe8B7F970d6E62E69BE74362fdD5F281D'
    },
  },
  compilers: {
    solc: {
      version: "0.8.3",
    },
  },
};