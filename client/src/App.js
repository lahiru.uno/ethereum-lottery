import React, { useState } from 'react'
import Web3 from 'web3';
import { lotteryAbi } from './abi/abis';
import './App.css';
import Manager from './Manager';
import Client from './Client';

// let web3 = new Web3(Web3.givenProvider);
// let web3 = new Web3('http://127.0.0.1:7545')
const web3 = new Web3(Web3.givenProvider || 'http://127.0.0.1:7545');
console.log(web3)

// const initWeb3 = () => {
//   return new Promise((resolve, reject) => {
//     if(typeof window.ethereum !== 'undefined') {
//       const web3 = new Web3(window.ethereum);
//       window.ethereum.enable()
//         .then(() => {
//           resolve(
//             new Web3(window.ethereum)
//           );
//         })
//         .catch(e => {
//           reject(e);
//         });
//       return;
//     }
//     if(typeof window.web3 !== 'undefined') {
//       return resolve(
//         new Web3(window.web3.currentProvider)
//       );
//     }
//     resolve(new Web3('http://localhost:7545'));
//   });
// };

const contractAddr = '0x23ac5448b4Eb4B7F630DAa97bc21af738B32B890';
const Lottery = new web3.eth.Contract(lotteryAbi, contractAddr);

function App() {

  const [manager, setManager] = useState('')
  const [isManagerError, setIsManagerError] = useState(false)

  const [playerIndex, setPlayerIndex] = useState(0)
  const [playerResponse, setPlayerResponse] = useState('')
  const [isPlayerError, setIsPlayerError] = useState(false)

  const [balanceIndex, setBalanceIndex] = useState(0)
  const [balanceResponse, setBalanceResponse] = useState('')
  const [isBalanceError, setIsBalanceError] = useState(false)

  const [ballotResponse, setBallotResponse] = useState('')
  const [ballotValue, setBallotValue] = useState(0)
  const [isBallotError, setIsBallotError] = useState(false)

  const [pickWinnerResponse, setPickWinnerResponse] = useState('')
  const [isPickWinnerError, setIsPickWinnerError] = useState(false)

  const [winner, setWinner] = useState('')
  const [isWinnerError, setIsWinnerError] = useState(false)

  const [contractBalance, setContractBalance] = useState(0)

  const handleReadManager = async (e) => {
    e.preventDefault();
    try{
      const result = await Lottery.methods.manager().call()
      console.log(result)
      setManager(`${result}`)
      setIsManagerError(false)
    }catch(err){
      console.log(err)
      setManager('Ooops... there was an error while trying to read manager')
      setIsManagerError(true)
    }    
  }

  const handleReadWinner = async (e) => {
    e.preventDefault();
    try{
      const result = await Lottery.methods.winner().call()
      console.log(result)
      setWinner(`Last winner index = ${result}`)
      setIsWinnerError(false)
    }catch(err){
      console.log(err)
      setWinner('Ooops... there was an error while trying to read manager')
      setIsWinnerError(true)
    }    
  }

  const handleReadPlayer = async (e) => {
    e.preventDefault();
    try{
      const result = await Lottery.methods.players(playerIndex).call()
      console.log(result)
      setPlayerResponse(`Player ${playerIndex} address : ${result}`)
      setIsPlayerError(false)
    }catch(err){
      console.log(err)
      setPlayerResponse(`Ooops... there was an error while trying to read user ${playerIndex}`)
      setIsPlayerError(true)
    }    
  }

  const handleSubmitBallot = async (e) => {
    e.preventDefault();  
    try{
      const accounts = await window.ethereum.enable();
      console.log(accounts[0])
      const response = await Lottery.methods.submitBallot()
        .send({from: accounts[0], value: web3.utils.toWei(ballotValue.toString()) })
      setBallotResponse(`Ballot submitted successfully`)
      setIsBallotError(false)
      const balance = await Lottery.methods.getBalance().call()
      setContractBalance(balance)
    }catch(err){
      setBallotResponse(`Ooops... there was an error while trying to submit ballot...`)
      setIsBallotError(true)
      console.log(err)
    }    
  }

  const handlePickWinner = async (e) => {
    e.preventDefault();  
    try{
      const accounts = await window.ethereum.enable();
      console.log(accounts[0])
      const response = await Lottery.methods.pickWinner().send({from: accounts[0]})
      setPickWinnerResponse(`Winner picked and all Ethers transferred to winning account`)
      setIsPickWinnerError(false)
      const balance = await Lottery.methods.getBalance().call()
      setContractBalance(balance)
    }catch(err){
      setPickWinnerResponse(`Ooops... there was an error while trying to pick winner...`)
      setIsPickWinnerError(true)
      console.log(err)
    }    
  }

  const handleGetPlayerBalance = async (e) => {
    e.preventDefault();  
    try{
      const response = await Lottery.methods.getPlayersBalance(balanceIndex).call()
      console.log(response)
      setBalanceResponse(`Player ${balanceIndex} Balance = ${response / 1000000000000000000}`)
      setIsBalanceError(false)      
    }catch(err){
      setBalanceResponse(`Ooops... there was an error while trying to palyer ${balanceIndex} balance!`)
      setIsBalanceError(true)
      console.log(err)
    }    
  }

  const [profile, setProfile] = useState('Client');

  function changeProfile(profile){   
    setProfile(profile);
  }

  return (
    <div className="App">
      <div className='app-side-bar'> 
        <div className='side-bar-header-area'>
          <span className='side-bar-header'>Participants</span>
        </div>
        <div className='side-button-area'>
          <button className={`side-button ${profile == 'Client' ? 'client' : ''}`} onClick={() => changeProfile('Client')}>
              Client
          </button>
        </div>
        <div className='side-button-area'>
          <button className={`side-button ${ profile == 'Manager' ? 'manager' : ''}`} onClick={() => changeProfile('Manager')}>
              Manager
          </button>
        </div>
      </div>
      <div className='app-main-container'>
        {(profile == 'Client')?  <Client></Client> : <Manager></Manager>}
        
      </div>
      
    </div> 
  );
}

export default App;
