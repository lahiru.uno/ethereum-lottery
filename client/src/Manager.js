import React, { useState } from 'react'
import Web3 from 'web3';
import { lotteryAbi } from './abi/abis';
import './Manager.css';

const web3 = new Web3(Web3.givenProvider || 'http://127.0.0.1:7545');
console.log(web3)

const contractAddr = '0x546Be749C372544792e18A880E1C8d5a1Ff12816';
const Lottery = new web3.eth.Contract(lotteryAbi, contractAddr);

function Manager() {
  const [manager, setManager] = useState('')
  const [isManagerError, setIsManagerError] = useState(false)

  const [playerIndex, setPlayerIndex] = useState(0)
  const [playerResponse, setPlayerResponse] = useState('')
  const [isPlayerError, setIsPlayerError] = useState(false)

  const [balanceIndex, setBalanceIndex] = useState(0)
  const [balanceResponse, setBalanceResponse] = useState('')
  const [isBalanceError, setIsBalanceError] = useState(false)

  const [ballotResponse, setBallotResponse] = useState('')
  const [ballotValue, setBallotValue] = useState(0)
  const [isBallotError, setIsBallotError] = useState(false)

  const [pickWinnerResponse, setPickWinnerResponse] = useState('')
  const [isPickWinnerError, setIsPickWinnerError] = useState(false)

  const [winner, setWinner] = useState('')
  const [isWinnerError, setIsWinnerError] = useState(false)

  const [contractBalance, setContractBalance] = useState(0)

  const [ballot, setBallots] = useState([])

  var event = Lottery.events.NewBallot(function (error, result) {
    if (!error) {
      console.log(result.returnValues._client);
      var client = result.returnValues._client
      setBallots([...ballot, client]);
    }
  });

  const handleReadManager = async (e) => {
    e.preventDefault();
    try {
      const result = await Lottery.methods.manager().call()
      console.log(result)
      setManager(`${result}`)
      setIsManagerError(false)
    } catch (err) {
      console.log(err)
      setManager('Ooops... there was an error while trying to read manager')
      setIsManagerError(true)
    }
  }

  const handleReadWinner = async (e) => {
    e.preventDefault();
    try {
      const result = await Lottery.methods.winner().call()
      console.log(result)
      setWinner(`Last winner index = ${result}`)
      setIsWinnerError(false)
    } catch (err) {
      console.log(err)
      setWinner('Ooops... there was an error while trying to read manager')
      setIsWinnerError(true)
    }
  }

  const handleReadPlayer = async (e) => {
    e.preventDefault();
    try {
      const result = await Lottery.methods.players(playerIndex).call()
      console.log(result)
      setPlayerResponse(`${result}`)
      setIsPlayerError(false)
    } catch (err) {
      console.log(err)
      setPlayerResponse(`Ooops... there was an error while trying to read user ${playerIndex}`)
      setIsPlayerError(true)
    }
  }

  const handleSubmitBallot = async (e) => {
    e.preventDefault();
    try {
      const accounts = await window.ethereum.enable();
      console.log(accounts[0])
      const response = await Lottery.methods.submitBallot()
        .send({ from: accounts[0], value: web3.utils.toWei(ballotValue.toString()) })
      setBallotResponse(`Ballot submitted successfully`)
      setIsBallotError(false)
      const balance = await Lottery.methods.getBalance().call()
      setContractBalance(balance)
    } catch (err) {
      setBallotResponse(`Ooops... there was an error while trying to submit ballot...`)
      setIsBallotError(true)
      console.log(err)
    }
  }

  const handlePickWinner = async (e) => {
    e.preventDefault();
    try {
      const accounts = await window.ethereum.enable();
      console.log(accounts[0])
      const response = await Lottery.methods.pickWinner().send({ from: accounts[0] })
      setPickWinnerResponse(`Winner picked and all Ethers transferred to winning account`)
      setIsPickWinnerError(false)
      const balance = await Lottery.methods.getBalance().call()
      setContractBalance(balance)
    } catch (err) {
      setPickWinnerResponse(`Ooops... there was an error while trying to pick winner...`)
      setIsPickWinnerError(true)
      console.log(err)
    }
  }

  const handleGetPlayerBalance = async (e) => {
    e.preventDefault();
    try {
      const response = await Lottery.methods.getPlayersBalance(balanceIndex).call()
      console.log(response)
      setBalanceResponse(`Player ${balanceIndex} Balance = ${response / 1000000000000000000}`)
      setIsBalanceError(false)
    } catch (err) {
      setBalanceResponse(`Ooops... there was an error while trying to palyer ${balanceIndex} balance!`)
      setIsBalanceError(true)
      console.log(err)
    }
  }

  return (
    <div className='main-container'>
      <div className='column header-column'>
        <span className='title'>Ethereum Lottery</span>
      </div>
      <div className='column-wrapper'>
      <div className='column ballot-column'>
          <span className='title'>Ballots</span>
            {ballot.map(item => {
              return <div className='row row20'><span className='ballot-address'>{item} </span></div>
            })}
        </div>
        <div className='column'>
          <span className='title'>Manager</span>
          <div className='row row100'>
            <span className='header'> Pick Winner</span>
            <div className='input-area'>
              <button className='submit-button' onClick={handlePickWinner}>Pick Winner</button>
            </div>
            <span className={`response ${isPickWinnerError ? 'error-message' : 'success-message'}`}>
              {pickWinnerResponse}
            </span>
          </div>

          <div className='row row100'>
            <span className='header'>Last Winner Index</span>
            <div className='input-area'>
              <button className='submit-button' onClick={handleReadWinner}>Last Winner</button>
            </div>
            <span className={`response ${isWinnerError ? 'error-message' : 'success-message'}`}>
              {winner}
            </span>
          </div>
          <div className='row row130'>
            <div className='header'> Read Player</div>
            <div className='input-area'>
              <input className='input-text'
                type="number"
                name="id"
                value={playerIndex}
                onChange={e => setPlayerIndex(e.target.value)}
                placeholder='Enter weights vector' />
              <button className='submit-button' onClick={handleReadPlayer}>Read player</button>
            </div>
            <span className={`response ${isPlayerError ? 'error-message' : 'success-message'}`}>
              {playerResponse}
            </span>
          </div>

          <div className='row row130'>
            <span className='header'>Player Balance</span>
            <div className='input-area'>
              <input className='input-text'
                type="number"
                name="player-index"
                value={balanceIndex}
                onChange={e => setBalanceIndex(e.target.value)} />
              <button onClick={handleGetPlayerBalance} className='submit-button green'>
                Player Balance
                            </button>
            </div>
            <span className={`response ${isBalanceError ? 'error-message' : 'success-message'}`}>
              {balanceResponse}
            </span>
          </div>
        </div>
      </div>
    </div>
  )

}

export default Manager;